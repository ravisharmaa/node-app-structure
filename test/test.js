const { add, sub } = require('../src/application/User/Repository/user.repository');
const request = require("supertest");
const app = require("../../src/app");

describe('add', () => {
  it('should return 3 when adding 1  to 2', () => {
    expect(add(1, 2)).toBe(3)
  })
})

describe('subtract', () => {
  it('should return 5 when subtracting 5 from 10', () => {
    expect(sub(10, 5)).toBe(5)
  })
})



// describe("Test the root path", () => {
//   test("It should response the GET method", done => {
//     request(app)
//       .get("/")
//       .then(response => {
//         expect(response.statusCode).toBe(200);
//         done();
//       });
//   });
// });