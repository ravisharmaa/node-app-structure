const authRouter = require('express').Router();
const {register, login } = require('../../../src/application/User/controller/user.controller')

authRouter.post('/register', register);
authRouter.post('/login', login);

module.exports = authRouter;

