const healthCheck = require('./app.routes');
const authRouter = require('./auth/auth.routes');
exports.registerRoutes = (app) => {
    handleHealthCheckRoute(app);
    handleAuthenticationRoutes(app);
};
function handleAuthenticationRoutes(app) {
    app.use('/auth', authRouter)
}
// handles health check.
function handleHealthCheckRoute(app) {
    app.use('/', healthCheck);
};