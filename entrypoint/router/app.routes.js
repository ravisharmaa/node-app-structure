const { response } = require('express');
const router = require('express').Router();

router.get('/health-check', async (request, response) => {
    return response.status(200).json({'status': 'Ok'});
});

module.exports = router;