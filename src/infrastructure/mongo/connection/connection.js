const mongoose = require('mongoose');
const DB_HOST = process.env.DB_HOST;
const DB_USER = process.env.DB_USER;
const DB_PASSWORD = process.env.DB_PASSWORD;
const AUTH_SOURCE = process.env.AUTH_SOURCE;
const DEFAULT_DB = process.env.DATABASE_NAME;

exports.connectToDatabase = ()  => {
    const url = `${DB_HOST}/${DEFAULT_DB}`;
    mongoose.connect(url, {
        authSource: AUTH_SOURCE,
        user: DB_USER,
        pass: DB_PASSWORD,
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
}