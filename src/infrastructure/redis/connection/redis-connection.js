const redis = require('redis');
const REDIS_HOST = process.env.REDIS_HOST;
const REDIS_PORT = process.env.REDIS_PORT;
let redisClient;

exports.connectToRedisClient = async () => {
    redisClient = redis.createClient({
        socket: {
            host: REDIS_HOST,
            port: REDIS_PORT
        }
    });
    await redisClient.connect();
}