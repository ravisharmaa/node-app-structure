const { connectToRedisClient } = require('./redis/connection/redis-connection');
const { connectToDatabase } = require('./mongo/connection/connection');

exports.connectToDatabase = () => {
    try {
        connectToDatabase()
    } catch(error) {
        throw new Error(error);
    }
}

exports.bootRedisConnection = async () => {
    try {
       await connectToRedisClient();
    }  catch(error) {
        throw new Error('Could not connect to redis because', error.message);
    }
};
