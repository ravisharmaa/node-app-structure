const { response } = require("express");
const { saveUser, findByUsername } = require("../Repository/user.repository");
const bcrypt = require("bcrypt");

exports.register = async (request, response) => {
    const { username, email, password } = request.body;
    try {
        await saveUser({
            username, email, password
        });
        return response.status(200).json({
            'status': 'User created successfully'
        });

    } catch(error) {
        return response.status(422).json({
            'status': 'Could not create recoed'
        })
    }
}

exports.login = async (request, response) => {
    const { username, password } = request.body;
    try {
        const user = await findByUsername({username});
        if (!user) {
            return response.status(401).json({
                'status': 'fail',
                'message': 'Wrong username'
            });    
        }
        const match = await bcrypt.compare(password, user.password);
        if (!match) {
            return response.status(401).json({
                'status': 'fail',
                "message": "Wrong password"
            });
        } 

        return response.status(200).json({
            'status': 'Authenticated',
            user
        });
    } catch(error) {
        return response.status(422).json({
            'status': 'Could not create recoed'
        })
    }
}