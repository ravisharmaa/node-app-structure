const bcrypt = require("bcrypt");
const userModel = require("../../../domain/User/UserDto");

exports.saveUser = async (registrationData) => {
    const passwordHash = await bcrypt.hash(registrationData.password, 10);
    const userData = await userModel.create({
        username: registrationData.username,
        email: registrationData.email,
        password: passwordHash
    });

    return userData;
}

exports.findByUsername = async(userName) => {
    const user = await userModel.findOne({userName});
    return user;
}