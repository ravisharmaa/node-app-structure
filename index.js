const process = require('process')
const PORT = process.env.PORT || 3000;
const APP_NAME = process.env.APP_NAME;
const { connectToDatabase, bootRedisConnection} = require('./src/infrastructure');
const app = require('./entrypoint/server');

Promise.all([connectToDatabase(), bootRedisConnection()])
    .then(() => {
        app.listen(PORT, () => {
            console.log(`${APP_NAME} listening on ${PORT}`);
        })        
    }).catch(error => {
        console.log(error);
    })

    process.on("unhandledRejection", err => {
        console.log(`An error occurred: ${err.message}`)
        server.close(() => process.exit(1))
    })